data "aws_db_instance" "all" {
  #for_each = toset([for db_instance in data.aws_db_instance.all : db_instance.id])

  db_instance_identifier = "database-1"
}

data "aws_s3_bucket" "default" {
  bucket = "tmdcsnapshotstest"
}

data "aws_iam_role" "default" {
  name = "tmdctestiamrole"
}


data "aws_kms_key" "default" {
  key_id = "9478f42a-d633-4cb2-88a2-ed7c6621b94c"
}


resource "aws_db_snapshot" "all" {
  #for_each = toset([for db_instance in data.aws_db_instance.all : db_instance.id])

  db_instance_identifier = data.aws_db_instance.all.id
  db_snapshot_identifier = "PRD-${formatdate("YYYY-MM-DD-HH-mm-ss", timestamp())}"
  tags = {
    Name = "PRD-snapshot"
  }
}

resource "aws_rds_export_task" "default" {
  export_task_identifier = aws_db_snapshot.all.db_instance_identifier
  source_arn             = aws_db_snapshot.all.db_snapshot_arn
  s3_bucket_name         = data.aws_s3_bucket.default.id
  iam_role_arn           = data.aws_iam_role.default.arn
  kms_key_id             = data.aws_kms_key.default.arn
}